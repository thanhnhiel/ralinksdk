U-Boot for the RT5350 with restore function from a USB flash drive.

(Tested on a Chinese clone a5-v11, 32M RAM, 4M FLASH, without the battery,
the bar, with Qualcomm interface)

For 32MB RAM version (most routers): file: uboot_usb_256_03.img
for 16MB RAM: file: uboot_usb_128_03.img

Compiled from various available sources with revisions / additions
for the router and the SOC:
1. Added support for SPI flash drive which is a clone, no error
message in the console.

2. Added support for loading firmware from a USB flash drive.
To do this on a flash drive to FAT32 format in the first partition should be
written firmware with name: firmware.bin
Connect a USB flash drive to the device, boot with holding the button reset.
During the flash firmware will light, then flash the blue LED light.

3. If not found the firmware file on a flash drive or flash drive
unrecognizable running tftp server on the device.
You can flash firmware over the tftp protocol from your computer with
the address of the subnet 192.168.1.0/24.
Command from Windows: tftp.exe -i 192.168.1.1 PUT firmware.bin
from Linux: tftp -m binary 192.168.1.1 -c put firmware.bin

4. You can restore firmware by Asus Firmware Restoration Utility,
which for their routers.

5. When connecting the console available extended menu of 10 items.

