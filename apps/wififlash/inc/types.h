/****************************************************************************
* Copyright (C) Robert Bosch Car Multimedia GmbH, 2018
* This software is property of Robert Bosch GmbH. Unauthorized
* duplication and disclosure to third parties is prohibited.
***************************************************************************/

/*!
*\file     types.h
*\brief    This header file contains the information related to functions
*           used in type handling.
*
*\author   RBVH/ENG2
*
*\par Copyright:
*(c) 2018 Robert Bosch Car Multimedia GmbH
*
*\par History:
* See history of revision control system
***************************************************************************/

#ifndef __TYPES_H__
#define __TYPES_H__

#include <stdint.h>

/* Define Maximum Path length */
#define MAX_STRING_LENGTH 256

#define DBLOG_USE_UART 0
#define DBLOG_LOG_PATH  "/tmp/logger.log"
#define DBLOG_LOG2_PATH  "/tmp/logger-2.log"
#define LIMIT_SIZE 512  // KB


#ifdef FALSE
#undef FALSE
#endif
#define FALSE (0x0)

#ifdef TRUE
#undef TRUE
#endif
#define TRUE (0x1)

#ifdef FAILED
#undef FAILED
#endif
#define FAILED (0x1)

#ifdef SUCCESS
#undef SUCCESS
#endif
#define SUCCESS (0x0)

#ifdef NULL
#undef NULL
#endif
#define NULL (0)

#ifdef __BOOL
#undef __BOOL
#endif
#define __BOOL uint32_t

#ifdef __RESULT
#undef __RESULT
#endif
#define __RESULT uint32_t

#endif
