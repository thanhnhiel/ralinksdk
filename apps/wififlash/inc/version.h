/*******************************************************************************
* Copyright (C) Robert Bosch Car Multimedia GmbH, 2018
* This software is property of Robert Bosch GmbH. Unauthorized
* duplication and disclosure to third parties is prohibited.
*******************************************************************************/

/*!
*\file     version.h
*\brief    This file keeps track of the changes of logging application.
*
*\author   RBVH/ENG23_RNT2HC
*
*\par Copyright:
*(c) 2018 Robert Bosch Car Multimedia GmbH
*
*\par History:
* See history of revision control system
*******************************************************************************/

#ifndef __VERSION_H__
#define __VERSION_H__

/**
 * @brief   Logging version
 */
#define LOGGING_MAJOR_VERSION              1
#define LOGGING_MINOR_VERSION              0
#define LOGGING_PATCH_VERSION              0

/**
 * @brief   Logging build time
 */
#define LOGGING_BUILD_TIMESTAMP            __DATE__ " " __TIME__

/**
 * @page    History Release History
 */

/**
 *******************************************************************************
 * @page        History
 *
 * @section     Release_1
 * @version     1.0.0
 * @date        2018-10-01
 *
 * @note
 *      + This is the 1st release
 *
 * @warning
 *      + None
 *
 * @details
 *      + Basic function such as : logging, uploading
 *      + Log type: QXDM, Kernel, MCU, JVM_Crash log
 *******************************************************************************
 */
 
#endif

