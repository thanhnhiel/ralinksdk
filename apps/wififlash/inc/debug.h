#ifndef _DEBUG_H_
#define _DEBUG_H_

#include <stdio.h>
#include "version.h"
#include "dblog.h"


#define DEBUG_INFO_ENABLED    (1)
#define DEBUG_WARN_ENABLED    (1)

#define LOG_COLOR_ERROR             "\x1B[31m"      /* Red */
#define LOG_COLOR_WARNING           "\x1B[33m"      /* Yellow */
#define LOG_COLOR_INFO              "\x1B[36m"      /* Blue */
#define LOG_COLOR_NORMAL            "\x1B[39m"      /* White */
  
#if 0
#if (DBLOG_USE_UART)
  #define LOG_COLOR_ERROR             "\x1B[31m"      /* Red */
  #define LOG_COLOR_WARNING           "\x1B[33m"      /* Yellow */
  #define LOG_COLOR_INFO              "\x1B[36m"      /* Blue */
  #define LOG_COLOR_NORMAL            "\x1B[39m"      /* White */
#else
  #define LOG_COLOR_ERROR              ""
  #define LOG_COLOR_WARNING            ""
  #define LOG_COLOR_INFO               ""
  #define LOG_COLOR_NORMAL             ""
#endif
#endif

#ifndef PRINTF
#define PRINTF dblog_printf
#endif

#ifndef MODULE_NAME
#define MODULE_NAME "No Define"
#endif


#define PRINTF_WHITECOLOR(x...) \
    do { \
     	dblog_printf (LOG_COLOR_NORMAL); \
        dblog_printf(x); \
    } while ( 0 )
        
#define PRINTF_BLUECOLOR(x...) \
    do { \
    	  dblog_printf (LOG_COLOR_INFO); \
        dblog_printf(x); \
        dblog_printf (LOG_COLOR_NORMAL); \
    } while ( 0 )
        
#define PRINTF_REDCOLOR(x...) \
    do { \
    	  dblog_printf (LOG_COLOR_ERROR); \
        dblog_printf(x); \
        dblog_printf (LOG_COLOR_NORMAL); \
    } while ( 0 )

#define PRINTF_YELLOWCOLOR(x...) \
    do { \
    	  dblog_printf (LOG_COLOR_WARNING); \
        dblog_printf(x); \
        dblog_printf (LOG_COLOR_NORMAL); \
    } while ( 0 )
        
#define PRINTF_COLOR(x...) \
    do { \
    	  dblog_printf (LOG_COLOR_WARNING); \
        dblog_printf(x); \
        dblog_printf (LOG_COLOR_NORMAL); \
    } while ( 0 )


#if (DEBUG_INFO_ENABLED) /* Enable information debug */
#define PUTS_DEBUG(x...) \
    do { \
        dblog_printf(x); \
    } while ( 0 )
#else
#define PUTS_DEBUG(x...)
#endif /* Enable information debug */

#if (DEBUG_INFO_ENABLED) /* Enable information debug */
#define PUTC_DEBUG(c) \
    do { \
        dblog_printf("%c", c); \
    } while ( 0 )
#else
#define PUTC_DEBUG(x...)
#endif /* Enable information debug */

#if (DEBUG_INFO_ENABLED) /* Enable information debug */
#define INFO_DEBUG(x...) \
    do { \
        dblog_printf("%s[%s][%s][%d][ INFO ] : ", LOG_COLOR_INFO, MODULE_NAME, __FILE__, __LINE__); \
        dblog_printf (x); \
        dblog_printf (LOG_COLOR_NORMAL); \
    } while ( 0 )
#else
#define INFO_DEBUG(x...)
#endif /* Enable information debug */

#if (DEBUG_WARN_ENABLED) /* Enable Warning */
#define WARN_DEBUG(x...)     \
    do { \
        dblog_printf("%s[%s][%s][%d][ WARNING ] : ", LOG_COLOR_WARNING, MODULE_NAME, __FILE__, __LINE__); \
        dblog_printf (x); \
        dblog_printf (LOG_COLOR_NORMAL); \
    } while ( 0 )
#else
#define WARN_DEBUG(x...)
#endif /* Enable Warning */

#endif
