#ifndef _WIFI_H_
#define _WIFI_H_

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include <signal.h>
#include <sys/stat.h>
#include <termios.h>    // POSIX terminal control definitions

// #define MODULE_NAME "GSM"
// #include "debug.h"
#include "utility.h"
#include "wifi.h"

//#define RING GPIOE(6)
#define WIFI_EN GPIOE(3)
#define WIFI_MODE GPIOA(0)

__RESULT wifi_init(void);
__RESULT wifi_deinit(void);
__RESULT wifi_initBL(void);
__RESULT wifi_deinitBL(void);


#endif
