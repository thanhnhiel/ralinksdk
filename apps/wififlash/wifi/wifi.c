#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <signal.h>
#include <sys/stat.h>

#define MODULE_NAME "WIFI"
#include "debug.h"
#include "utility.h"
#include "param.h"
#include "uart.h"
#include "wifi.h"
#include "gpio.h"

/*
    Linux - Old version MCU:
    MCU   Uart3 ->  uart0   :   sensor - 115200
    wifi    ->      uart1   9600
    GSM    ->       uart2   9600
*/

// WIFI
#define WIFI_BAUDRATE B115200
#define WIFI_BL_BAUDRATE B921600
static UARTINFO pUartWIFI;
static char wifi_dev_name[] = "/dev/ttyS1";
static void wifi_rx_handler(char * buf, uint32_t len);

// ttyGS0
#define TTYGS0_BAUDRATE B115200
static UARTINFO pUartGS0;
static char uartgs0_dev_name[] = "/dev/ttyGS0";

#define ClearRespondBuf() { respLen = 0; }


enum WIFI_STATE
{
    WIFI_STATE_NONE = 0,
    WIFI_STATE_INIT_AP,
//    WIFI_STATE_IDLE = 0,
    WIFI_STATE_CONNECTED,
    WIFI_STATE_CLOSED,
};

static enum WIFI_STATE wifi_state = WIFI_STATE_NONE;


static pthread_t thread_pid;
static volatile __BOOL enable_wifi_thread;

static char respond[MAX_UART_RECEIVE_SIZE];
static volatile uint16_t respLen = 0;

//==========================================================
static void uartgs0_rx_handler(char * buf, uint32_t len);
static void* wifi_process_thread(void* arg);



void initGPIO(void)
{
    gpio_export(WIFI_EN);
    gpio_dir_out(WIFI_EN);
    
    gpio_export(WIFI_MODE);
    gpio_dir_out(WIFI_MODE);

    gpio_set_value(WIFI_EN, 0);   // Turn off
    gpio_set_value(WIFI_MODE, 0); // 0 : BL, 1: Normal
}

void deinitGPIO(void)
{
    gpio_dir_out(WIFI_EN);
    gpio_dir_out(WIFI_MODE);
    gpio_set_value(WIFI_EN, 0);   // Turn off
    
    gpio_unexport(WIFI_MODE);
    gpio_unexport(WIFI_EN);
}

// USB_TTYGS0
static void uartgs0_bl_rx_handler(char * buf, uint32_t len)
{
  if (wifi_state == WIFI_STATE_CONNECTED)
  {
    //WARN_DEBUG("ttyGS0 send %d bytes\n", len);
    uart_write(&pUartWIFI, (uint8_t *)buf, len);
    printf("host send %d bytes\n", len);
  }
}

// USB_TTYGS0
static void wifi_bl_rx_handler(char * buf, uint32_t len)
{
  if (wifi_state == WIFI_STATE_CONNECTED)
  {
    //WARN_DEBUG("ttyGS0 send %d bytes\n", len);
    uart_write(&pUartGS0, (uint8_t *)buf, len);
    printf("wifi send %d bytes\n", len);
  }
}

__RESULT wifi_initBL(void)
{
    __RESULT ret;
    
    WARN_DEBUG("initGPIO...\n");   
    initGPIO();
        
    gpio_set_value(WIFI_EN, 0);   // Turn off
    sleep(1);
    // 0: BL mode
    gpio_set_value(WIFI_MODE, 0); // 0 : BL, 1: Normal
    //gpio_set_value(WIFI_MODE, 1); // 0 : BL, 1: Normal
    sleep(1);
    gpio_set_value(WIFI_EN, 1);   // Turn on
    
    WARN_DEBUG("wifi_init...\n");
    
    // Uart pUartWIFI
    pUartWIFI.dev_name = wifi_dev_name;
    pUartWIFI.baud_rate = B115200;
    
    pUartWIFI.uart_rx_handler = &wifi_bl_rx_handler;
    pUartWIFI.u32_rxBufSize = MAX_UART_RECEIVE_SIZE;
    
    ret = uart_initialize(&pUartWIFI);
    if (ret != SUCCESS)
    {
        WARN_DEBUG("initialize pUartWIFI failed\n");
        return FAILED;
    }
        
    // Uart pUartGS0
    pUartGS0.dev_name = uartgs0_dev_name;
    pUartGS0.baud_rate = B115200; //TTYGS0_BAUDRATE;
    
    pUartGS0.uart_rx_handler = &uartgs0_bl_rx_handler;
    pUartGS0.u32_rxBufSize = MAX_UART_RECEIVE_SIZE;
  
    ret = uart_initialize(&pUartGS0);
    if (ret != SUCCESS)
    {
        WARN_DEBUG("initialize pUartGS0 failed\n");
        return FAILED;
    }
    
    wifi_state = WIFI_STATE_CONNECTED;
    
    WARN_DEBUG("wifi_init Done\n");
    return ret;
}


__RESULT wifi_deinitBL(void)
{
  enable_wifi_thread = FALSE;
  
  deinitGPIO();
  uart_deinitialize(&pUartGS0);  
  return uart_deinitialize(&pUartWIFI);    
  
}



__RESULT wifi_init(void)
{
  __RESULT ret = SUCCESS;

  WARN_DEBUG("wifi_init...\n");

  // Uart pUartWIFI
  pUartWIFI.dev_name = wifi_dev_name;
  pUartWIFI.baud_rate = WIFI_BAUDRATE;

  pUartWIFI.uart_rx_handler = &wifi_rx_handler;
  pUartWIFI.u32_rxBufSize = MAX_UART_RECEIVE_SIZE;

  ret = uart_initialize(&pUartWIFI);
  if (ret != SUCCESS)
  {
    WARN_DEBUG("initialize pUartWIFI failed\n");
    return FAILED;
  }


  // Uart pUartGS0
  pUartGS0.dev_name = uartgs0_dev_name;
  pUartGS0.baud_rate = TTYGS0_BAUDRATE;
  
  pUartGS0.uart_rx_handler = &uartgs0_rx_handler;
  pUartGS0.u32_rxBufSize = MAX_UART_RECEIVE_SIZE;
  
  ret = uart_initialize(&pUartGS0);
  if (ret != SUCCESS)
  {
    WARN_DEBUG("initialize pUartGS0 failed\n");
    return FAILED;
  }
  
  enable_wifi_thread = TRUE;
  if (pthread_create(&thread_pid, 
                    NULL, &wifi_process_thread, &pUartWIFI) != 0)
  {
      WARN_DEBUG(("Failed to spawn WIFI receive read thread\n"));
      return FAILED;
  }

  
  return SUCCESS;
}

/*
CMD
$$$
ERR: ?-Cmd
<1.00> set wlan join 7
OK
<1.00> set wlan channel 1
OK
<1.00> set apmode ssid iDL-VLogger
OK
<1.00> set ip dhcp 4
OK
<1.00> set ip address 192.168.1.10
OK
<1.00> set ip net 255.255.255.0
OK
<1.00> set ip gateway 192.168.1.1
OK
<1.00> set ip proto 0x2
OK
<1.00>
       set ip host 192.168.1.11
OK
<1.00>
       set ip remote 10000
OK
<1.00>
       save
Storing in config
<1.00>
       reboot
*Reboot*wifly-FZX Ver: 1.00 Build: r1638, Jun  3 2015 16:08:55 on RN1723
MAC Addr=00:1e:c0:65:82:56
*READY*
AP mode as iDL-VLogger on chan 1
Listen on 2000
DHCP Server Init
DHCP: 192.168.1.20 lease to DEL
DHCP: 192.168.1.20 lease to DEL
*OPEN*HELLO123HELLO123*CLOS*

*/

static const char *mode_ap_wificmd[] = 
{
  "set wlan join 7\r",
  "set wlan channel 1\r",
  "set apmode ssid iDL-VLogger\r",
  "set ip dhcp 4\r", 
  "set ip address 192.168.1.10\r",
  "set ip net 255.255.255.0\r", 
  "set ip gateway 192.168.1.1\r",
  "set ip proto 0x2\r\n",
  "set ip host 192.168.1.11\r\n",  
  "set ip remote 10000\r\n", 
  "save\r\n",
  "reboot\r\n",
  0
};


static void* wifi_process_thread(void* arg)
{
  UARTINFO* pUart = (UARTINFO *)arg;
  __BOOL inited = FALSE;
  //char str[MAX_STRING_LENGTH];
  uint8_t i = 0;
  uint16_t len = 0;
  
  INFO_DEBUG("wifi_process started\n");
  while (enable_wifi_thread == TRUE)
  {
    if (inited == FALSE)
    {
      wifi_state = WIFI_STATE_INIT_AP;
      
      INFO_DEBUG("Enter Command Mode\n");
      ClearRespondBuf();
      uart_write(pUart, (uint8_t *)"$$$", 3);
      sleep(1);
      len = respLen;
      respond[len] = 0;
      
      // Not found
      if (strstr(respond, "CMD") == 0) 
      {
        ClearRespondBuf();
        uart_write(pUart, (uint8_t *)"\r", 1);
        sleep(1);
        
        len = respLen; respond[len] = 0;        
        if (strstr(respond, "ERR: ?-Cmd") == 0) 
        {
          PRINTF("respond failed: %s", respond);
          ClearRespondBuf();
          sleep(10);
          continue;          
        }
      }
      PRINTF("respond: %s\n", respond);
      
      i=0;
      while (mode_ap_wificmd[i] != '\0')
      {
        //strcpy(str, "set wlan join 7\r");
        INFO_DEBUG("%s\n", mode_ap_wificmd[i]);
        ClearRespondBuf();
        uart_write(pUart, (uint8_t *)mode_ap_wificmd[i], strlen(mode_ap_wificmd[i]));
        sleep(1);
        
        len = respLen; respond[len] = 0;         
        if ((strstr(respond, "\r\nOK\r\n") == 0) && 
               (strstr(respond, "Storing in config") == 0) &&
               (strstr(respond, "*Reboot*") == 0)) 
        {
          PRINTF("respond failed: %s", respond);
          ClearRespondBuf();
          sleep(10);
          continue;      
        }
        PRINTF("respond: %s\n", respond);
        i++;
      }
        
      inited = TRUE;
      wifi_state = WIFI_STATE_CLOSED;
      PRINTF("Wifi init done\n");
    }
    else
    {
      sleep(1);
    }
  }
  INFO_DEBUG("wifi_process Ended\n");
  return 0;
}

__RESULT wifi_deinit(void)
{
  enable_wifi_thread = FALSE;
  
  pthread_cancel(thread_pid);
  
  if(pthread_join(thread_pid, NULL) == 0)
  {
    INFO_DEBUG("wifi_process_thread successfully closed\n");
  }
  
  uart_deinitialize(&pUartGS0);  
  return uart_deinitialize(&pUartWIFI);
}

static char wifi_line1[7];

void putline1(char c)
{
    uint8_t i;
    
    for (i=0;i<5;i++)
    {
        wifi_line1[i] = wifi_line1[i+1];
    }
    wifi_line1[5] = c;
    wifi_line1[6] = 0;
}

__BOOL Wifi_IsClosed()
{
    if (strcmp(wifi_line1, "*CLOS*") == 0)
    {
        return TRUE;
    }
    return FALSE;
}

__BOOL Wifi_IsOpened()
{
    if (strcmp(wifi_line1, "*OPEN*") == 0)
    {
        return TRUE;
    }
    return FALSE;
}
    


// WIFI
static void wifi_rx_handler(char *buf, uint32_t len)
{
  uint16_t i;
  char str[MAX_UART_RECEIVE_SIZE];
  uint16_t strLen;
  
//    WARN_DEBUG("u2 send %d bytes\n", len);
  
  strLen = 0;
  
  for (i=0;i<len;i++)
  {
    //if (wifi_state == WIFI_STATE_CLOSED || 
      //    wifi_state == WIFI_STATE_CONNECTED)
    //{
      putline1(buf[i]);
      
      if (Wifi_IsClosed() == TRUE)
      {
        wifi_state = WIFI_STATE_CLOSED;
        PRINTF("Socket closed\n");
        continue;
      }
      else if (Wifi_IsOpened() == TRUE)
      {
        wifi_state = WIFI_STATE_CONNECTED;
        PRINTF("Socket opened\n");
        continue;
      }
    //}
    
    switch(wifi_state)
    {
      case WIFI_STATE_INIT_AP:
        if (respLen < (MAX_UART_RECEIVE_SIZE + 1))
        {
          respond[respLen] = buf[i];
          respLen++;
        }
      break;
      
      case WIFI_STATE_CONNECTED:
        str[strLen] = buf[i];
        strLen++;
      break;
      
      case WIFI_STATE_CLOSED:
      break;         
      
      default:
      break;
    }
  }

  if (strLen) 
  {
    uart_write(&pUartGS0, (uint8_t *)str, strLen);
    strLen = 0;
  }
}

// USB_TTYGS0
static void uartgs0_rx_handler(char * buf, uint32_t len)
{
  if (wifi_state == WIFI_STATE_CONNECTED)
  {
    //WARN_DEBUG("ttyGS0 send %d bytes\n", len);
    uart_write(&pUartWIFI, (uint8_t *)buf, len);
  }
}
