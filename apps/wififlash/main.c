#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include <signal.h>
#include <sys/stat.h>

#define MODULE_NAME "WIFI TEST"


volatile sig_atomic_t flagExitApp = 0;

void sigint_handler(int sig)
{
    (void)sig;
    flagExitApp = 1; // set flag
    //INFO_DEBUG ("Got Break signal\n");
}

int main(int argc, char **argv)
{
    int ret = 1;
    
    // Register signals
    signal(SIGINT, sigint_handler);

    printf("Hello\n");

    while (flagExitApp==0)
    {
        sleep(1);
    }

    
    return ret;
}


