#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include <signal.h>
#include <sys/stat.h>

#define MODULE_NAME "WIFI TEST"


volatile sig_atomic_t flagExitApp = 0;

void hotPluglerHandler(int sig)
{
    (void)sig;
    printf("hotPluglerHandler\n");
    system("echo 'hotPluglerHandler' >> /tmp/log.txt");
}



void sigint_handler(int sig)
{
    (void)sig;
    printf("sigint_handler\n");
    system("echo 'sigint_handler' >> /tmp/log.txt");
}

void dhcpcHandler(int sig)
{
    (void)sig;
    printf("dhcpcHandler\n");
    system("echo 'dhcpcHandler' >> /tmp/log.txt");
}

int main(int argc, char **argv)
{
    int ret = 1;
    
    // Register signals
    signal(SIGINT, sigint_handler);
	signal(SIGTSTP, dhcpcHandler);
	//signal(SIGUSR2, SIG_IGN);
	signal(SIGTTIN, hotPluglerHandler);
    

    printf("Hello\n");
    system("echo 'Hello' >> /tmp/log.txt");
    while (flagExitApp==0)
    {
        sleep(1);
    }

    
    return ret;
}


