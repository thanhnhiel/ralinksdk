Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
255.255.255.255 0.0.0.0         255.255.255.255 UH    1      0        0 eth2
192.168.1.0     0.0.0.0         255.255.255.0   U     0      0        0 eth2
12.0.0.0        0.0.0.0         255.0.0.0       U     0      0        0 ra0
0.0.0.0         192.168.1.100   0.0.0.0         UG    0      0        0 eth2
