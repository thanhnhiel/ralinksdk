/* This is a generated file, don't edit */

#define NUM_APPLETS 47

const char applet_names[] ALIGN1 = ""
"[" "\0"
"[[" "\0"
"ash" "\0"
"brctl" "\0"
"cat" "\0"
"chpasswd" "\0"
"cp" "\0"
"date" "\0"
"depmod" "\0"
"echo" "\0"
"expr" "\0"
"free" "\0"
"grep" "\0"
"halt" "\0"
"ifconfig" "\0"
"init" "\0"
"init" "\0"
"insmod" "\0"
"kill" "\0"
"killall" "\0"
"login" "\0"
"ls" "\0"
"lsmod" "\0"
"mkdir" "\0"
"modprobe" "\0"
"mount" "\0"
"ping" "\0"
"poweroff" "\0"
"printf" "\0"
"ps" "\0"
"pwd" "\0"
"reboot" "\0"
"rm" "\0"
"rmmod" "\0"
"route" "\0"
"sed" "\0"
"sh" "\0"
"sleep" "\0"
"syslogd" "\0"
"telnetd" "\0"
"test" "\0"
"top" "\0"
"touch" "\0"
"udhcpc" "\0"
"udhcpd" "\0"
"uptime" "\0"
"vconfig" "\0"
;

int (*const applet_main[])(int argc, char **argv) = {
test_main,
test_main,
ash_main,
brctl_main,
cat_main,
chpasswd_main,
cp_main,
date_main,
depmod_main,
echo_main,
expr_main,
free_main,
grep_main,
halt_main,
ifconfig_main,
init_main,
init_main,
insmod_main,
kill_main,
kill_main,
login_main,
ls_main,
lsmod_main,
mkdir_main,
modprobe_main,
mount_main,
ping_main,
halt_main,
printf_main,
ps_main,
pwd_main,
halt_main,
rm_main,
rmmod_main,
route_main,
sed_main,
ash_main,
sleep_main,
syslogd_main,
telnetd_main,
test_main,
top_main,
touch_main,
udhcpc_main,
udhcpd_main,
uptime_main,
vconfig_main,
};
const uint16_t applet_nameofs[] ALIGN2 = {
0x0000,
0x0002,
0x0005,
0x0009,
0x000f,
0x0013,
0x001c,
0x001f,
0x0024,
0x002b,
0x0030,
0x0035,
0x003a,
0x003f,
0x0044,
0x004d,
0x0052,
0x0057,
0x005e,
0x0063,
0x806b,
0x0071,
0x0074,
0x007a,
0x0080,
0x0089,
0x408f,
0x0094,
0x009d,
0x00a4,
0x00a7,
0x00ab,
0x00b2,
0x00b5,
0x00bb,
0x00c1,
0x00c5,
0x00c8,
0x00ce,
0x00d6,
0x00de,
0x00e3,
0x00e7,
0x00ed,
0x00f4,
0x00fb,
0x0102,
};
#define MAX_APPLET_NAME_LEN 8
